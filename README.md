## Monaros: a ROS-based Mona Robot control library
This package is developed for the higher-level control of the base of Mona robot which has [Ardumona](https://gitlab.com/MonaCS/ardumona.git) installed.

### Preparation
#### 1. Setting up the master computer (Part 1)
1. Make sure you have a computer with Ubuntu 16.04 (or equivalent) installed. It is used as the master computer.
2. Install ROS Kinetic on your computer - follow [this instruction](http://wiki.ros.org/kinetic/Installation/Ubuntu).
3. Create a ROS workspace and build it - follow [this instruction](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment).
4. Make sure that the source is updated, use the following command to edit using Terminal.
    ```
    ~$ cd
    ~$ sudo nano .bashrc
    ```
    and then write this command at the last line of *.bashrc* file
    ```
    ~$ source ~/catkin_ws/devel/setup.bash
    ```
    Save and close the file.
5. Close the Terminal.

#### 2. Setting up the Raspberry Pi
1. Make sure that the Raspberry Pi Zero W has the Raspbian Stretch installed.
2. Open a Terminal and install ROS Kinetic on the Raspberry Pi Zero W using the following commands:
    ```
    sudo apt-get update
    sudo apt-get install -y build-essential gdebi
    mkdir -p ~/tmp
    wget https://github.com/nomumu/Kinetic4RPiZero/releases/download/v_2017-10-15/rpi-zerow-kinetic_1.0.0-1_armhf.zip
    unzip rpi-zerow-kinetic_1.0.0-1_armhf.zip
    sudo gdebi rpi-zerow-kinetic_1.0.0-1_armhf.deb
    sudo /opt/ros/kinetic/initialize.sh
    ```
3. Create a ROS workspace and build it following [this instruction](http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment).
4. Make sure that the source is updated. Use the following command to update using Terminal:
    ```
    ~$ cd
    ~$ sudo nano .bashrc
    ```
    and then write this command at the last line of *.bashrc* file
    ```
    ~$ source ~/catkin_ws/devel/setup.bash
    ```
    Save and close the file.
5. Close the Terminal.
6. Open a new Terminal. Clone this [Monaros repository](https://gitlab.com/MonaCS/monaros) to the *src* of *catkin_ws* directory. Follow this command to do it:
    ```
    ~$ cd ~/catkin_ws/src
    ~$ git clone https://gitlab.com/MonaCS/monaros.git
    ~$ cd ..
    ~$ catkin_make
    ```
6. Open a new Terminal, and update the *wpa_suplicant.conf* file to define a local Wifi connection. Using the following command
    ```
    ~$ sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
    ```
    and then write 
    ```
    network={
        ssid="<NAME_OF_THE_LOCAL_SSID>"
        psk="<PASSWORD_OF_THE_LOCAL_SSID>"
        key_mgmt=WPA-PSK
    }
    ```
    In the case of connecting the Raspberry Pi and Vicon positioning system to the local router using *TP-LINK_F0E112* whose password is *1FF0E112*, as in the joint lab, write:
    ```
    network={
        ssid="TP-LINK_F0E112*"
        psk="1FF0E112*"
        key_mgmt=WPA-PSK
    }
    ```
    Save and close the file.
7. Close the terminal.
8. Restart the Raspberry Pi.

#### 3. Setting up the master computer (Part 2)
1. Connect the computer to the same network with Raspberry Pi and Vicon whose SSID is *TP-LINK_F0E112* and password is *1FF0E112*.
2. Check the IP Address of the master computer, namely <IP_COMPUTER>.
3. Open the *.bashrc* file on the computer again using
    ```
    ~$ cd
    ~$ sudo nano .bashrc
    ```
    and then write this command at the last line of it
    ```
    ~$ export ROS_HOSTNAME=<IP_COMPUTER>
    ~$ export ROS_MASTER_URI=http://<IP_PICOMPUTER>:11311
    ```
    with <IP_COMPUTER> obtained from the previous command.
    Save and close the file, and then close the Terminal.
4. Scan the IP Addresses connected to the network to find which IP Address is assigned to the Raspberry Pi, namely <IP_PIZERO>. You can use the following command:
    ```
    ~$ nmap -sn 192.168.1.0/24
    ```
5. Open the *.bashrc* file on the Raspbeery Pi again. **If you access the Raspberry Pi via SSH from the computer, use:**
    ```
    ~$ ssh pi@<IP_PIZERO>
    ```
    **and type**
    ```
    ~$ cd
    ~$ sudo nano .bashrc
    ```
    **and then write this command at the last line of it**
    ```
    ~$ export ROS_HOSTNAME=<IP_PIZERO>
    ~$ export ROS_MASTER_URI=http://<IP_PICOMPUTER>:11311
    ```
    **with <IP_COMPUTER> and <IP_PIZERO> obtained from the previous command.
    Save and close the file, and then close the Terminal.**

### Operating procedures
1. On the master computer:
    1. Ensure that a ROSMASTER is running on the master computer using:
        ```
        roscore
        ```
    2. **LET THE TERMINAL OPEN AND THE ROSMASTER KEEPS RUNNING.**
2. On the Raspberry Pi Zero W
    1. Use the following procedures if you use SSH to connect to Raspberry Pi Zero W: 
        Open a Terminal on your master computer and write
        ```
        ~$ ssh pi@<IP_PIZERO>
        ```
        Otherwise, just directly open the Terminal on the Raspberry Pi.
    2. Change the mode of *monamain.py* rosnode in the *pysrc* directory using
        ```
        ~$ cd ~/catkin_ws/src/monaros/pysrc
        ~$ chmod +x monamain.py
        ```
    3. Run the *monamain.py* using
        ```
        ~$ rosrun monaros monamain.py
        ```
    4. **NEVER CLOSE THE TERMINAL IF YOU ARE STILL WORKING WITH THE RASPBERRY PI ZERO W OF THE MONA ROBOT.**

### Development instruction
1. You can create a new Python script inside the *pysrc* directory where your program is going to be written to.

### Communication protocol
1. To communicate with Mona Robot, a serial communication is used with the following configuration:
    - Baudrate: 38400
    - Serial output which is fed to Mona Robot:
        ```
        L<linear_velocity>$A<angular_velocity>$
        ```
        Note that, to avoid unstable system, the linear and angular velocty are bounded to be -2500<=linear_velocity<=2500 and -650<=angular_velocity<=650.
    - *The serial input from Mona Robot has not been considered here since no sensor data is required so far*
2. To obtain command from the program on the master computer, the program subscribes a rosmessage published on a rostopic published by the a rosnode in the master computer. In this case, the linear and angular velocity inputs are in the rostopic namely
    ```
    /mona0/cmd_vel
    ```
    where its declaration can be found in */monaros/pysrc/monamain.py*, i.e.,
    ```
    subRobotInfo = rospy.Subscriber("mona0/cmd_vel", geoTwist, command_callback)
    ```
    If you are working with more than one robot, make sure that each robot has unique rostopic, e.g., change the rostopic in the declaration to be
    ```
    /mona1/cmd_vel
    ```
    for robot 1.
    If a rosnode is publishing a message to the rostopic, you can directly check the information in a new terminal on Raspberry Pi using
    ```
    ~$ rostopic echo /mona0/cmd_vel
    ```
3. The rostopic contains a rosmessage, namely *geometry_msgs/Twist* (see [here](http://docs.ros.org/api/geometry_msgs/html/msg/Twist.html) for more details). In the case of using *monamain.py*, the linear velocity is retrieved from the variable
    ```
    command.linear.x
    ```
    while the angular velocity
    ```
    command.angular.z
    ```

### Developers
1. Hilton Tnunay (htnunay@gmail.com)
2. Zhenhong Li