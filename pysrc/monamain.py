#!/usr/bin/env python
import rospy
import math
import serial
import sys
import time

from geometry_msgs.msg import Twist as geoTwist
from std_msgs.msg import Float32MultiArray as stdFloat32
from std_msgs.msg import String as strInfo

ser = serial.Serial(port='/dev/serial0', baudrate=38400, timeout=3)

command = geoTwist()


def command_callback(cmd_data):
    global command
    command = cmd_data


def read_from_ardumona():
    in_data = ser.readline()
    str_data = str(in_data)
    if (ser.in_waiting <= 0):
        ser.flushInput()
    return str_data


def int_saturation(_input, _max, _min):
    if (_input > _max):
        _input = _max
    elif (_input < _min):
        _input = _min
    return int(_input)


def main(args):
    rospy.init_node('monamainpy', anonymous=True)
    rate = rospy.Rate(60)  # 10-80 hz rate due to the PID leverl in mona

    # rename the index of the rostopics following the index of robot this monaros is installed on
    subRobotInfo = rospy.Subscriber("mona0/cmd_vel", geoTwist, command_callback)
    pubCommand = rospy.Publisher("mona0/low_information", strInfo, queue_size=1)

    rospy.loginfo("READY")
    while not rospy.is_shutdown():
        t_start = rospy.get_time()
        temp_word_ser = '$L%05d$A%04d$' % (int_saturation(int(command.linear.x), 99999, -99999), int_saturation(int(command.angular.z), 9999, -9999))
        # the linear velocity is limited to 5 digital numbers,
        # angular velocity is limited to 4 digital numbers
        write_ser = temp_word_ser.encode('utf-8')
        ser.write(write_ser)
        if (ser.out_waiting <= 0):
            ser.flushOutput()

        read_serial_data = read_from_ardumona()
        pubCommand.publish(read_serial_data)
        #rospy.loginfo(read_serial_data)
        t_now = rospy.get_time() - t_start
        rate.sleep()


if __name__ == '__main__':
    main(sys.argv)
