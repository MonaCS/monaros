#include "ros/ros.h"
#include <wiringPi.h>
#include <wiringSerial.h>

int main(int argc, char **argv){
	ros::init(argc, argv, "monamain");
	ros::NodeHandle nh;
	ros::Rate rate(20);
/*
	int serial_id;

	if((serial_id = serialOpen("/dev/ttyS0", 4800))<0){
		fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
		return 1;
	}
*/
	while(ros::ok()){
/*		while (serialDataAvail(serial_id)){
			printf(" Data -> %d", serialGetchar (serial_id));
		}
*/
		ros::spinOnce();
		rate.sleep();
	}
}
